import React from "react";

const AuthContextState = React.createContext({
  isLoggedIn: false,
  onLogOut: () => {},
  onLogIn: (email, password) => {},
});

export default AuthContextState;