import React, { useImperativeHandle } from "react";
import { useRef } from "react/cjs/react.development";
import styles from "./Input.module.css";
import PropTypes from "prop-types";

// labelText
// name
// type
// value
// onChange
// onBlur
// ref
// min?
// max?
// step?
// isValid?
// errorMessage?

const Input = React.forwardRef((props, ref) => {
  const inputRef = useRef();

  const activate = () => {
    inputRef.current.focus();
  };

  useImperativeHandle(ref, () => {
    return {
      focus: activate,
    };
  });

  return (
    <div
      className={`${styles.control} ${
        props.isValid === false ? styles.invalid : ""
      }`}
    >
      <label htmlFor={props.htmlFor}>{props.labelText}</label>
      <input
        ref={inputRef}
        name={props.name}
        type={props.type}
        value={props.value}
        min={props.min}
        max={props.max}
        step={props.step}
        onChange={props.onChange}
        onBlur={props.onBlur}
        onFocus={props.onFocus}
        placeholder={props.placeholder}
        required={props.required}
      />
      {!props.isValid && <p className={styles.error}>{props.errorMessage}</p>}
    </div>
  );
});

Input.propTypes = {
  labelText: PropTypes.string,
}

export default Input;
