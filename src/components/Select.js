import React from "react";
import styles from "./Select.module.css";

const Select = (props) => {
  return (
    <div
      className={`${styles.control} ${props.className}`}
    >
      <label htmlFor={props.htmlFor}>{props.labelText}</label>
      <select
        className={styles.control}
        name={props.name}
        value={props.value}
        onChange={props.onChange}
      >
        {props.options.map((option) => (
          <option key={option.id} value={option.id}>
            {option.name}
          </option>
        ))}
      </select>
    </div>
  );
};

export default Select;
