import React from "react";

const AuthContextReducer = React.createContext({
  isLoggedIn: false,
  onLogOut: () => {},
  onLogIn: (email, password) => {},
});

export default AuthContextReducer;