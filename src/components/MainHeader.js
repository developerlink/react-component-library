import React, { useContext } from "react";
import { NavLink } from "react-router-dom";
import styles from "./MainHeader.module.css";
import AuthContextState from "../store-usestate/authContextState";

const MainHeader = () => {
  const authCtx = useContext(AuthContextState);

  const logoutHandler = () => {
  };

  return (
    <header className={styles.header}>
      <nav>
        <ul>
          <NavLink className={styles.home} to="/">
            Home
          </NavLink>
          <li>
            <NavLink to="/form-usestate">Form useState</NavLink>
          </li>
          <li>
            <NavLink to="/form-useinput">Form useInput</NavLink>
          </li>
          <li>
            <NavLink to="/form-react-select">Form react-select</NavLink>
          </li>
          <li>
            <NavLink to="/chartbar">ChartBar</NavLink>
          </li>
          <li>
            <NavLink to="/modal">Modal</NavLink>
          </li>
          <li>
            <NavLink to="/table">Table</NavLink>
          </li>
          <li>
            <NavLink to="/list">List</NavLink>
          </li>
          <li>
            <NavLink to="/login-usestate">Login useState</NavLink>
          </li>
          <li>
            <NavLink to="/login-usereducer">Login useReducer</NavLink>
          </li>
          <li>
            <NavLink to="/login-redux">Login Redux</NavLink>
          </li>
          {authCtx.isLoggedIn && (
            <button onClick={logoutHandler} className={styles.headerBtn}>
              Logout
            </button>
          )}
        </ul>
      </nav>
    </header>
  );
};

export default MainHeader;
