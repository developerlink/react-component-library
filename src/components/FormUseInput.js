import React, { useEffect, useRef, useState } from "react";
import Button from "./Button";
import styles from "./FormUseInput.module.css";
import Input from "./Input";
import useInput from "./useInput";
import Select from "./Select";

// Every html element has the 'key' and 'ref' prop

const DUMMY_SELECTIONS = [
  {
    id: 1,
    name: "Toyota",
  },
  {
    id: 2,
    name: "Mitsubishi",
  },
  {
    id: 3,
    name: "Mazda",
  },
  {
    id: 4,
    name: "Volkswagen",
  },
  {
    id: 5,
    name: "Tesla",
  },
];

const validateTitle = (value) => {
  if (value === "") {
    return {
      valueIsValid: false,
      errorMessage: "The title is missing",
    };
  } else if (value.length > 10) {
    return {
      valueIsValid: false,
      errorMessage: "The title is longer than 10 characters",
    };
  } else {
    return {
      valueIsValid: true,
    };
  }
};

const validateAmount = (value) => {
  if (+value < 1 || 100 < +value) {
    return {
      valueIsValid: false,
      errorMessage: "The amount must be between 1 and 100",
    };
  } else {
    return {
      valueIsValid: true,
    };
  }
};

const validateDate = (value) => {
  if (value === "") {
    return {
      valueIsValid: false,
      errorMessage: "The date must be filled",
    };
  } else {
    return {
      valueIsValid: true,
    };
  }
};

const FormUseInput = () => {
  const {
    value: titleValue,
    valueIsValid: titleIsValid,
    hasError: titleHasError,
    errorMessage: titleErrorMessage,
    valueChangeHandler: titleChangeHandler,
    valueBlurHandler: titleBlurHandler,
    setIsTouched: setTitleIsTouched,
    reset: resetTitle,
    hasChanged: titleHasChanged,
  } = useInput(validateTitle);
  const {
    value: amountValue,
    valueIsValid: amountIsValid,
    hasError: amountHasError,
    errorMessage: amountErrorMessage,
    valueChangeHandler: amountChangeHandler,
    valueBlurHandler: amountBlurHandler,
    setIsTouched: setAmountIsTouched,
    reset: resetamount,
  } = useInput(validateAmount, 0);
  const {
    value: dateValue,
    valueIsValid: dateIsValid,
    hasError: dateHasError,
    errorMessage: dateErrorMessage,
    valueChangeHandler: dateChangeHandler,
    valueBlurHandler: dateBlurHandler,
    setIsTouched: setDateIsTouched,
    reset: resetdate,
  } = useInput(validateDate);
  const { 
    value: selectionValue, 
    valueChangeHandler: selectionChangeHandler 
  } = useInput();

  const titleInputRef = useRef();
  const amountInputRef = useRef();
  const dateInputRef = useRef();
  const submitButtonRef = useRef();
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [showSubmitMessage, setShowSubMessage] = useState(false);
  const [submitMessage, setSubmitMessage] = useState("");

  // Focus on first input on page load.
  useEffect(() => {
    titleInputRef.current.focus();
  }, []);

  useEffect(() => {
    if (titleHasChanged) {
      setShowSubMessage(false);
    }
  }, [titleHasChanged]);

  const onSubmitHandler = (event) => {
    event.preventDefault();

    submitButtonRef.current.focus();

    // Shows error messages. Does not works as input validity in the form validity!
    setTitleIsTouched(true);
    setAmountIsTouched(true);
    setDateIsTouched(true);

    let formIsValid = titleIsValid && amountIsValid && dateIsValid;

    if (formIsValid) {
      setIsSubmitting(true);
      setShowSubMessage(false);

      let data = {
        title: titleValue,
        amount: amountValue,
        date: dateValue,
        carId: selectionValue
      };

      // Simulating loading effect before setting things
      setTimeout(() => {
        setIsSubmitting(false);
        setShowSubMessage(true);
        setSubmitMessage(
          <h3 className={styles.success}>Submitted: {JSON.stringify(data)}</h3>
        );
        resetTitle();
        resetamount();
        resetdate();
        titleInputRef.current.focus();
      }, 1000);

      setTitleIsTouched(false);
      setAmountIsTouched(false);
      setDateIsTouched(false);
    } else if (titleHasError) {
      titleInputRef.current.focus();
    } else if (amountHasError) {
      amountInputRef.current.focus();
    } else if (dateHasError) {
      dateInputRef.current.focus();
    }
  };

  return (
    <React.Fragment>
      <h2>Form with custom hook useInput</h2>
      {showSubmitMessage ? submitMessage : "Enter new data"}
      <form onSubmit={onSubmitHandler}>
        <Input
          labelText="Title"
          name="title"
          type="text"
          placeholder="Title"
          value={titleValue}
          onChange={titleChangeHandler}
          onBlur={titleBlurHandler}
          isValid={!titleHasError}
          errorMessage={titleErrorMessage}
          ref={titleInputRef}
        />

        <Input
          labelText="Amount"
          name="amount"
          type="number"
          min="1"
          max="100"
          step="1"
          value={amountValue}
          onChange={amountChangeHandler}
          onBlur={amountBlurHandler}
          isValid={!amountHasError}
          errorMessage={amountErrorMessage}
          ref={amountInputRef}
        />

        <Input
          labelText="Date"
          name="date"
          type="date"
          min="2019-01-01"
          max="2022-12-31"
          value={dateValue}
          onChange={dateChangeHandler}
          onBlur={dateBlurHandler}
          isValid={!dateHasError}
          errorMessage={dateErrorMessage}
          ref={dateInputRef}
        />

        <Select
          labelText="Car Makes"
          name="car-makes"
          value={selectionValue}
          onChange={selectionChangeHandler}
          options={DUMMY_SELECTIONS}
        />

        <Button className={styles.btn} ref={submitButtonRef} type="submit">
          {isSubmitting ? (
            <i className="fa fa-circle-o-notch fa-spin"></i>
          ) : (
            "Submit"
          )}
        </Button>
      </form>
    </React.Fragment>
  );
};

export default FormUseInput;
