import React, { useState, useEffect } from "react";
import AuthContextState from "./authContextState";

const AuthContextStateProvider = (props) => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  useEffect(() => {
    const storedUserLoggedInInformation = localStorage.getItem("isLoggedIn");

    if (storedUserLoggedInInformation === "1") {
      setIsLoggedIn(true);
    }
  }, []);

  const loginHandler = (email, password) => {
    if (email && password) {
      // Some validation from database.
      localStorage.setItem("isLoggedIn", "1");
      setIsLoggedIn(true);
    }
  };

  const logoutHandler = () => {
    console.log("logout is called");
  };

  // const logoutHandler = () => {
  //   console.log("logout is called");
  //   localStorage.removeItem("isLoggedIn");
  //   setIsLoggedIn(false);
  // };

  return (
    <AuthContextState.Provider
      value={{
        isLoggedIn: isLoggedIn,
        onLogout: logoutHandler,
        onLogIn: loginHandler,
      }}
    >
      {props.children}
    </AuthContextState.Provider>
  );
};

export default AuthContextStateProvider;
