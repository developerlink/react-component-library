import React, { useEffect, useRef, useState } from "react";
import Button from "./Button";
import styles from "./FormReactSelect.module.css";
import useInput from "./useInput";
import Select from "react-select";
import CreatableSelect, { useCreatable } from "react-select/creatable";

// npm i --save react-select

const DUMMY_CARS = [
  { id: "p1", name: "Toyota" },
  { id: "p2", name: "Mitsubishi" },
  { id: "p3", name: "Mazda" },
  { id: "p4", name: "Volkswagen" },
  { id: "p5", name: "Tesla" },
];

const DUMMY_FOOD = [
  { id: 1, name: "chocolate" },
  { id: 2, name: "strawberry" },
  { id: 3, name: "vanilla" },
  { id: 4, name: "banana" },
  { id: 5, name: "mango" },
];

const DUMMY_TAGS = [
  { id: 1, name: "action" },
  { id: 2, name: "adventure" },
  { id: 3, name: "comedy" },
  { id: 4, name: "fantasy" },
  { id: 5, name: "sci-fi" },
];

const DUMMY_MOVIES = [
  { id: 1, name: "Titanic" },
  { id: 2, name: "Avengers" },
  { id: 3, name: "Braveheart" },
  { id: 4, name: "Back to the future" },
  { id: 5, name: "Forrest Gump" },
];

const customStyles = {
  option: (base) => ({
    ...base,
    color: "white",
    padding: 10,
    "&:hover": {
      color: "white",
    },
  }),
  multiValue: (base) => ({
    ...base,
    backgroundColor: "#222326",
  }),
  multiValueRemove: (base) => ({
    ...base,
    color: "white",
  }),
};

const customTheme = (theme) => {
  return {
    ...theme,
    colors: {
      ...theme.colors,
      neutral0: "#0B0C0D", // background color
      neutral20: "#3B3C40", // border color passive
      neutral30: "#A6A6A6", // border color hover
      neutral40: "gray", // No options color
      neutral50: "#A6A6A6", //placeholder
      neutral60: "gray", // arrow down color active
      neutral80: "silver", // single value, input, indicator
      primary: "#222326", // option selected, border in focus
      primary25: "#3B3C40", // option hover
      primary50: "silver", // option clicked
      danger: "black", // multivalue x
      dangerLight: "white", // multivalue x background
    },
  };
};

const validateSelectedFood = (value) => {
  if (value === undefined || value === "") {
    return {
      valueIsValid: false,
      errorMessage: "A food option must be selected!",
    };
  } else {
    return {
      valueIsValid: true,
    };
  }
};

const FormReactSelect = () => {
  // Set a default value if it is not nullable.
  const [selectedCar, setSelectedCar] = useState(DUMMY_CARS[4]);
  const {
    value: selectedFood,
    valueIsValid: selectedFoodIsValid,
    hasError: selectedFoodHasError,
    errorMessage: selectedFoodErrorMessage,
    valueBlurHandler: selectedFoodBlurHandler,
    setValue: setSelectedFood,
    setIsTouched: selectedFoodIsTouched,
    reset: resetSelectedFood,
  } = useInput(validateSelectedFood);

  const [selectedTag, setSelectedTag] = useState();
  const [selectedMovies, setSelectedMovies] = useState([]);

  const submitButtonRef = useRef();
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [showSubmitMessage, setShowSubMessage] = useState(false);
  const [submitMessage, setSubmitMessage] = useState("");


  const selectedMoviesChangeHandler = (options) => {
    setSelectedMovies(options);
  };

  const createMovieOptionHandler = (newOptionName) => {
    const newOptionId = DUMMY_MOVIES.at(-1).id + 1;
    const newOption = {
      id: newOptionId,
      name: newOptionName,
    };
    DUMMY_MOVIES.push(newOption);

    // Add this when it updates the UI. Right now I don't know how.
    // setSelectedMovies(prevValue => ([
    //   ...prevValue,
    //   newOption
    // ]));
  };

  const carChangeHandler = (event) => {
    setSelectedCar({
      id: event.target.value,
      name: event.target.label,
    });
  };

  const tagChangeHandler = (event) => {
    setSelectedTag(event.target.value);
  };

  useEffect(() => {
    console.log(selectedCar);
    console.log(selectedFood);
    console.log(selectedFoodIsValid);
  }, [selectedCar, selectedFood, selectedFoodIsValid]);

  const onSubmitHandler = (event) => {
    event.preventDefault();
    submitButtonRef.current.focus();

    // console.log(selectedCar);
    // console.log(selectedFood);
    console.log("Selected Movies: ", selectedMovies);

    let formIsValvalue = true;

    if (formIsValvalue) {
      setIsSubmitting(true);
      setShowSubMessage(false);

      let data = {};

      // Simulating loading effect before setting things
      setTimeout(() => {
        setIsSubmitting(false);
        setShowSubMessage(true);
        setSubmitMessage(
          <h3 classlabel={styles.success}>Submitted: {JSON.stringify(data)}</h3>
        );
      }, 1000);
    }
  };

  return (
    <React.Fragment>
      <h2>Form with npm package react-select</h2>
      {showSubmitMessage ? submitMessage : "Enter new data"}
      <form onSubmit={onSubmitHandler}>
        <label>Standard select with default value</label>
        <Select
          value={selectedCar}
          onChange={setSelectedCar}
          options={DUMMY_CARS}
          getOptionLabel={(option) => option.name}
          getOptionValue={(option) => option.id}
          autoFocus={true}
        />

        <label>With validation and custom styling</label>
        <Select
          placeholder="Select food"
          value={selectedFood}
          onChange={setSelectedFood}
          onBlur={selectedFoodBlurHandler}
          options={DUMMY_FOOD}
          getOptionLabel={(option) => option.name}
          getOptionValue={(option) => option.id}
          styles={customStyles}
          theme={(theme) => customTheme(theme)}
        />
        {selectedFoodHasError && (
          <p className={styles.error}>{selectedFoodErrorMessage}</p>
        )}

        <label>Select multiple with custom styling</label>
        <Select
          value={selectedTag}
          // onChange={tagChangeHandler}
          options={DUMMY_TAGS}
          getOptionLabel={(option) => option.name}
          getOptionValue={(option) => option.id}
          styles={customStyles}
          theme={(theme) => customTheme(theme)}
          isMulti={true}
        />

        <label>Select multiple with create</label>
        <CreatableSelect
          isMulti={true}
          options={DUMMY_MOVIES}
          closeMenuOnSelect={false}
          onChange={selectedMoviesChangeHandler}
          getOptionLabel={(option) => option.name}
          getOptionValue={(option) => option.id}
          onCreateOption={createMovieOptionHandler}
        />

        <Button className={styles.btn} ref={submitButtonRef} type="submit">
          {isSubmitting ? (
            <i className="fa fa-circle-o-notch fa-spin"></i>
          ) : (
            "Submit"
          )}
        </Button>
      </form>
    </React.Fragment>
  );
};

export default FormReactSelect;
