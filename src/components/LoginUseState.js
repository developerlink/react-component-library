import React, {
  useState,
  useEffect,
  useReducer,
  useContext,
  useRef,
} from "react";
import styles from "./LoginUseState.module.css";
import Card from "./Card";
import Input from "./Input";
import AuthContextState from "../store-usestate/authContextState";

const LoginUseState = () => {
  const [enteredEmail, setEnteredEmail] = useState("");
  const [emailIsValid, setEmailIsValid] = useState();
  const [emailErrorMessage, setEmailErrorMessage] = useState("");
  const [enteredPassword, setEnteredPassword] = useState("");
  const [passwordIsValid, setPasswordIsValid] = useState();
  const [passwordErrorMessage, setPasswordErrorMessage] = useState("");
  const [formIsValid, setFormIsValid] = useState(false);

  const authCtx = useContext(AuthContextState);

  const emailInputRef = useRef();
  const passwordInputRef = useRef();

  useEffect(() => {
    const identifier = setTimeout(() => {
      setFormIsValid(emailIsValid && passwordIsValid);
    }, 500);

    return () => {
      clearTimeout(identifier);
    };
  }, [emailIsValid, passwordIsValid]);

  const emailChangeHandler = (event) => {
    setEnteredEmail(event.target.value);
  };

  const passwordChangeHandler = (event) => {
    setEnteredPassword(event.target.value);
  };

  const validateEmailHandler = (event) => {
    const { value } = event.target;
    if (value.includes("@")) {
      setEmailIsValid(true);
      setEmailErrorMessage("");
    } else {
      setEmailIsValid(false);
      setEmailErrorMessage("The email is invalid");
    }
  };

  const validatePasswordHandler = (event) => {
    const { value } = event.target;

    if (value.trim().length < 6) {
      setPasswordIsValid(true);
      setPasswordErrorMessage("");
    } else {
      setPasswordIsValid(false);
      setPasswordErrorMessage("The password is longer than 6 characters");
    }
  };

  const submitHandler = (event) => {
    event.preventDefault();
    if (formIsValid) {
      authCtx.onLogIn(enteredEmail, enteredPassword);
      console.log("success");
      authCtx.onLogOut();
      console.log("logout success");
    // } else if (!emailIsValid) {
    //   emailInputRef.current.focus();
    // } else {
    //   passwordInputRef.current.focus();
    }
  };

  const clickHandler = () => {
    authCtx.onLogOut();
    console.log("logout success");
  }

  return (
    <React.Fragment>
      <h2>Login useState</h2>
      {!authCtx.isLoggedIn && (
        <Card className={styles.login}>
          <form onSubmit={submitHandler}>
            <div className={styles.actions}>
              <Input
                labelText="Email"
                type="text"
                value={enteredEmail}
                onChange={emailChangeHandler}
                onBlur={validateEmailHandler}
                ref={emailInputRef}
                isValid={emailIsValid}
                errorMessage={emailErrorMessage}
              />
              <Input
                labelText="Password"
                type="text"
                value={enteredPassword}
                onChange={passwordChangeHandler}
                onBlur={validatePasswordHandler}
                ref={passwordInputRef}
                isValid={passwordIsValid}
                errorMessage={passwordErrorMessage}
              />
              <button className={styles.loginBtn} type="submit">
                Login
              </button>
            </div>
          </form>
        </Card>
      )}
      <button className={styles.loginBtn} onClick={clickHandler}>
                Logout
              </button>
    </React.Fragment>
  );
};

export default LoginUseState;
