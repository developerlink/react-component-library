import React, { useImperativeHandle } from "react";
import { useRef } from "react/cjs/react.development";
import styles from "./InputDataList.module.css";

// labelText
// name
// type
// value
// onChange
// onBlur
// ref
// min?
// max?
// step?
// isValid?
// errorMessage?

const InputDataList = React.forwardRef((props, ref) => {
  const inputRef = useRef();

  const activate = () => {
    inputRef.current.focus();
  };

  useImperativeHandle(ref, () => {
    return {
      focus: activate,
    };
  });

  return (
    <div
      className={`${styles.control} ${
        props.isValid === false ? styles.invalid : ""
      }`}
    >
      <label htmlFor={props.htmlFor}>{props.labelText}</label>
      <input
        ref={inputRef}
        name={props.name}
        type={props.type}        
        onChange={props.onChange}
        onBlur={props.onBlur}
        placeholder={props.placeholder}
        required={props.required}
        list="data"
      />
      <datalist id="data">
        <option data-value="1" value="Explorer" />
        <option data-value="2" value="Firefox" />
        <option data-value="3" value="Chrome" />
        <option data-value="4" value="Opera" />
        <option data-value="5" value="Safari" />
      </datalist>

      {!props.isValid && <p className={styles.error}>{props.errorMessage}</p>}
    </div>
  );
});

export default InputDataList;
