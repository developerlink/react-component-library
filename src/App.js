import React from "react";
import { Routes, Route, Navigate } from "react-router-dom";
import "./App.css";
import MainHeader from "./components/MainHeader";
import FormUseState from "./components/FormUseState";
import FormUseInput from "./components/FormUseInput";
import FormReactSelect from "./components/FormReactSelect";
import ChartBarExample from "./components/ChartBarExample";
import ModalExample from "./components/ModalExample";
import LoginUseState from "./components/LoginUseState";
import LoginUseReducer from "./components/LoginUseReducer";
import LoginRedux from "./components/LoginRedux";

function App() {
  return (
    <React.Fragment>
      <MainHeader />
      <Routes>
        <Route path="/" element={<Navigate replace to="/home" />} />        
        <Route path='/home' element={<Navigate replace to="/form-useinput" />} />
        <Route path='/form-usestate' element={<FormUseState />} />
        <Route path='/form-useinput' element={<FormUseInput />} />
        <Route path='/form-react-select' element={<FormReactSelect />} />
        <Route path='/chartbar' element={<ChartBarExample />} />
        <Route path='/modal' element={<ModalExample />} />
        <Route path="/login-usestate" element={<LoginUseState />} />
        <Route path="/login-usereducer" element={<LoginUseReducer />} />
        <Route path="/login-redux" element={<LoginRedux />} />
      {/* <UseFetchGetExample /> */}
      </Routes>
    </React.Fragment>
  );
}

export default App;
