import { useState } from "react";

// 1. parameter is a function that validates the value.
// 2. parameter is the default value.
// Both are optional. 

// Remember that validateValue parameter must return valueIsValid on all 
// return paths or it will throw this error: 
// TypeError: Cannot destructure property 'valueIsValid' of 'validateValue(...)' 
// as it is undefined. 

const useInput = (validateValue, defaultValue = "") => {
  const [value, setValue] = useState(defaultValue);
  const [isTouched, setIsTouched] = useState(false);
  const [hasChanged, setHasChanged] = useState(false);

  let valueIsValid;
  let errorMessage;
  let hasError = false;

  if (validateValue) {
    const { valueIsValid: validity, errorMessage: message } = validateValue(value);
    valueIsValid = validity;
    errorMessage = message;    
    if ((!valueIsValid && hasChanged) || (!valueIsValid && isTouched)) {
      hasError = true;
    }
  }

  const valueChangeHandler = (event) => {
    setValue(event.target.value);
    setHasChanged(true);
  };

  const valueBlurHandler = () => {
    setIsTouched(true);
  };

  const reset = () => {
    setValue(defaultValue);
    setIsTouched(false);
    setHasChanged(false);
  };

  return {
    value,
    valueIsValid,
    hasError,
    errorMessage,
    valueChangeHandler,
    valueBlurHandler,
    setValue, 
    setIsTouched,
    reset,
    hasChanged,
  };
};

export default useInput;
