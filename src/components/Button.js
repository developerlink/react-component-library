import React, { useImperativeHandle } from "react";
import { useRef } from "react/cjs/react.development";
import Styles from "./Button.module.css";


// className?
// type
// onClick

const Button = React.forwardRef((props, ref) => {
  const btnRef = useRef();

  const activate = () => {
    btnRef.current.focus();
  };
 
  useImperativeHandle(ref, () => {
    return {
      focus: activate,
    };
  });

  return (
    <button
    ref={btnRef}
      className={`${Styles.button} ${props.className}`}
      type={props.type || "button"}
      onClick={props.onClick}
    >{props.children}</button>
  );
});

export default Button;
